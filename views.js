function updateListView(people) {

  // creer un nouvel élement liste
  var liste = document.getElementById('people-list');

  // Purge de la liste
  // on supprime tous les neuds enfants.
  while (liste.firstChild) {
    liste.removeChild(liste.firstChild);
  }


  for (var i = 0; i < people.length; i++) {
    var newItem = document.createElement('li');

    // ajouter element dans la liste
    liste.appendChild(newItem);

    // on stock dans l'élément HTML l'index correspodant dans le modèle
    newItem.setAttribute('data-index', i);

    // mettre nom saisie dans element liste
    newItem.innerText = people[i].personne;
    //
    // var cotatisationElem = document.createElement('span');
    // cotatisationElem.innerText = ' ' + people[i].cotisation + '€'
    // newItem.appendChild(cotatisationElem);

    // Alternative
    newItem.innerHTML = people[i].personne + ' <span>' + people[i].cotisation + '€</span>';
    newItem.innerHTML += '<span>' + people[i].genre + '</span>';
    newItem.innerHTML += ' <span>' + people[i].addedAt + '</span>';

    var removeBtn = document.createElement('button');
    removeBtn.className = 'remove-item';
    removeBtn.innerText = 'X';
    removeBtn.onclick = function(event) {

      // MISE A JOUR DU MODEL
      // on récupère l'attribut data-index du parent (li) pour spécifier 
      // l'élément du tableau à supprimer (l'index).
      // Array.prototype.splice permet de supprimer un élément du tableau
      people.splice(
        event.target.parentElement.attributes.getNamedItem('data-index').value, // début
        1 // combien d'élément à supprimer
      );
      console.log('people ', people);

      // MISE A JOUR DE LA VUE
      // On va demander au parent du parent du bouton cliqué
      // (le ul donc), de supprmier le parent du bouton (li)
      // event.target.parentElement.parentElement.removeChild(event.target.parentElement);
      updateListView(people);
      updateHeader(people);

      // mise à jour bouton valid
      validBtn.disabled = false;
      document.getElementById('max-count-error').style.display = '';
    }

    newItem.appendChild(removeBtn);
  }
}




function updateListRecherche(rechercheNom) {

  // creer un nouvel élement liste
  var listeRecherche = document.getElementById('demo');

  // Purge de la liste
  // on supprime tous les neuds enfants.
  while (listeRecherche.firstChild) {
    listeRecherche.removeChild(listeRecherche.firstChild);
  }


  for (var i = 0; i < nomRecherche.length; i++) {
    var newSearch = document.createElement('li');

    // ajouter element dans la liste
    listeRecherche.appendChild(newSearch);

    // on stock dans l'élément HTML l'index correspodant dans le modèle
    newSearch.setAttribute('maRecherche', i);

    // mettre nom saisie dans element liste
    newSearch.innerText = nomRecherche[i].valeurNom;
    //
    // var cotatisationElem = document.createElement('span');
    // cotatisationElem.innerText = ' ' + people[i].cotisation + '€'
    // newItem.appendChild(cotatisationElem);

    // Alternative
    newSearch.innerHTML = nomRecherche[i].personnerecherche;
  }
}

// Gestion des invitée.
'use strict';

// les invités (modèle)
var people = [];

var countElement = document.getElementById('count');
var cotisationTotalElem = document.getElementById('total-cotisation');

// lors d'un click sur bouton
var validBtn = document.getElementById('valid-btn');
if (!validBtn) {
  console.error('Bouton validation manquant');
} else {
  validBtn.onclick = function(event) {

    if (count >= 10) {
      document.getElementById('max-count-error').style.display = 'block';
      event.target.disabled = true;
      return;
    }

    // récupère le nom saisie
    var inputName = document.getElementsByTagName('input')[0];
    if (!inputName) {
      console.error('champs de saisie manquant');
      // on arrête l'executuion de la fonction
      return;
    }
    var inputCotisation = document.getElementsByTagName('input')[1];
    var genre;
    var inputGenre = document.getElementsByName('genre')
      .forEach(function(input) {
        console.log('input', input);
        if (input.checked) {
          genre = input.value;
        }
      });

    // mise à jour du modèle
    var newInvit = {
      personne: inputName.value,
      cotisation: parseInt(inputCotisation.value),
      addedAt: new Date(),
      genre: genre
    };

    people.push(newInvit);
    console.log('people: ', people);

    updateListView(people);
    updateHeader(people);
  };
}

function updateHeader(people) {
  var totalCotisation = 0;
  for (var i = 0; i < people.length; i++) {
    totalCotisation = totalCotisation + people[i].cotisation;
  }

  countElement.innerText = people.length;
  cotisationTotalElem.innerText = totalCotisation;
}



var nomRecherche = [];

var validRecherche = document.getElementById('recherche-btn');
var valeurNom = document.getElementById("maRecherche");

if (!validRecherche) {
  console.error('Bouton recherche manquant');
} else {
  validRecherche.onclick = function(event) {

    // récupère le nom saisie
    var inputRecherche = document.getElementsByTagName('input')[0];
    if (!inputRecherche) {
      console.error('champs de recherche manquant');
      // on arrête l'executuion de la fonction
      return;
    }

    // mise à jour du modèle
    var newRecherche = {
      personnerecherche: valeurNom.value,
    };

    nomRecherche.push(newRecherche);
    console.log('recherche:', nomRecherche);

    updateListRecherche(nomRecherche);
  };
}




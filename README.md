# Liste d'invités

1. Créer une page avec un champs de saisie texte et un bouton de validation
1. Ajouter un conteneur de liste `<ul id="people"></ul>`
1. Lorsque l'utilisateur click sur le bouton de validation, faites en sorte que s'ajoute un élément dans la liste contenant le nom de la personne entré dans le champs de saisie, plus un bouton de suppression de cette élément. Par exemple : `<li>Jean-Pierre</li>`
1. Ajouter au dessus de la liste un en tête avec le nombre de personne dans la liste mise à jour à chaque ajout.
1. Si on arrive au maximum d'inviter (10 par exemple), alors on empêche l'ajout de nouvelles personne dans la liste.
1. EXTRA TOP NIVEAU : Ajouter la possibilité de retirer un invité.